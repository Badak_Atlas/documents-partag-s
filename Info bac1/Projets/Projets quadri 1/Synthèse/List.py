

"""
 -Les listes peuvent contenir des listes des stings des nombres
 -On peut les envoyers a des fonctions comme des parametres
 -POUR ACCEDER A UNE LISTE ON UTILISE [][][]
 -L'indice commence a zero, les indices doivent etres des int
 -Les indices peuvent etres un calcul genre list[8-5]
 -Dans ennumerate ou len les listes imbriqués comptent pour 1 élément
 -IN et not in sont des boléens qui compare l'élém a la listes
 -on peut mettre deux listes en une avec +
 -On peut afficher des segments entier grac a [1:4] laisser juste : met toute la liste ou tout ce qui reste
 -del liste[0:4]
 -liste.append(456) attention on ne peut donner que un argument a append liste.append(1,2) ne fonctionne pas
 -.join() et .split()
 """
-------------------------------------------------

horsemen = ["war", "famine", "pestilence", "death"]

for i in range(0,len(horsemen)):
    print(horsemen[i])

"""Juste un exemple simple de lecture de liste"""

-------------------------------------------------

tuple = (0,1,2,3,4)
liste = [0,2]
liste.extend(tuple)
liste.append(1)

print(tuple,liste)

"""comment transformer un tuple en list pour savoir le modifier"""

--------------------------------------------------

def f():
    s = "abcdefegh"
    return s[4:6] # il faut monter de 1 en plus comme dans in range , putain ca pue la merde
print(f())

"""Souligne le fait qu'il faille faire attention aux indices"""

--------------------------------------------------

"paramètres multiples = *"
def fonction(*parametres):#un nombre non définis de variables
    print(paramètres)
fonction("a","a","i","o")

"""Illustre comment créer une fonction qui accepte un nombre non définis de parametres nomées"""

--------------------------------------------------

students = [
    ("John", ["CompSci", "Physics"]),
    ("Vusi", ["Maths", "CompSci", "Stats"]),
    ("Jess", ["CompSci", "Accounting", "Economics", "Management"]),
    ("Sarah", ["InfSys", "Accounting", "Economics", "CommLaw"]),
    ("Zuki", ["Sociology", "Economics", "Law", "Stats", "Music"])]

# Count how many students are taking CompSci
counter = 0
for (name, subjects) in students:
    if "CompSci" in subjects:
           counter += 1

print("The number of students taking CompSci is", counter)

"""On peut itérer de cette manière des string en créant deux listes distingues mais ca na marche plus dés qu'il y a un int"""

--------------------------------------------------

a = [1, 2, 3]
>>> b = [4, 5, 6]
>>> c = a + b
>>> c
[1, 2, 3, 4, 5, 6]

""" exemple de liaison de deux listes"""

student_tuples = [('john', 'A', 15),('jane', 'B', 12),('dave', 'B', 10),]
print(sorted(student_tuples, key=lambda student: student[2]))

"""utilisation de sorted en changeant la key pour qu'elle trie notre tuple selon l'age"""

-----------------------------------------------
>>> class Student:
...     def __init__(self, name, grade, age):
...         self.name = name
...         self.grade = grade
...         self.age = age
...     def __repr__(self):
...         return repr((self.name, self.grade, self.age))
>>> student_objects = [
...     Student('john', 'A', 15),
...     Student('jane', 'B', 12),
...     Student('dave', 'B', 10),
... ]
>>> sorted(student_objects, key=lambda student: student.age)   # sort by age
[('dave', 'B', 10), ('jane', 'B', 12), ('john', 'A', 15)]

"""Ici même exemple mais en utilisatn des classes, sorted prend un troisième paramètres , reverse=True ou false(par def)"""

-----------------------------------------------------
def double_stuff(a_list):
    """ Return a new list which contains
        doubles of the elements in a_list that are not lower than 3
    """
    return [ 2 * value for value in a_list if a>= 3 ]
    """forme d'écriture abréjée de part return suivis des insteuctions"""
-----------------------------------------------------

string = "ceci est une phrase quelconque"
string_test = string.upper()
print(string,string_test)

if string > string_test:
    print("oui")
a = "a"
b = "A"

if a > b:
    print("wtf")

    """Les minuscules sont plus grandes que les maj"""

-----------------------------------------------------
liste = [1,[2,2,2,2,2,3],4,[5,[6,7]],8]
def flatten(l,empt_list):

    for elem in l:
        if type(elem) == list or type(elem) == tuple :
           flatten(elem,empt_list)

        else:
            empt_list.append(elem)

    l = empt_list
    return l
empt_list = []
print(flatten(liste,empt_list))


"""Ma version de flatten mais pour l'instant ont doit lui donner une liste vide ou pas blc elle ne la crée pas"""
--------------------------------------------------------------------------------

print(count(2,liste))
"""Une fonction count qui se sert de flatten"""

--------------------------------------------------------------------------------
