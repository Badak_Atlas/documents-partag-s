class Point:
    """Stock les coordonnées d'un point en x,y"""
    def __init__(self,x,y):
        """Constructeur du point"""
        self.x = x
        self.y = y
    def get_coordonnees(self):
        """Affiche les coordonnées du point"""
        print("Le point se situe en (",self.x,";",self.y,")")

    def __str__(self):
        return "({0}; {1})".format(self.x, self.y)

    def __add__(self,other):
        return Point(self.x + other.x, self.y + other.y)

    def __mul__(self, other):
        return Point(self.x * other.x , self.y * other.y)


    def __rmul__(self, point):
        return Point(point * self.x,  point * self.y)

point1 = Point(15,10)
point2 = Point(10,5)
point3 = point1 + point2
print(2 * point3)

class Cards:
    """Pas encore sur de l'utilité"""
    familles =("Carreau","Trefle","Coeur","Pique")
    rangs = ("Joker","Ace","Deux","Trois","Quatre","Cinq","Six","Sept",\
            "Huit","Neuf","Dix","Valet","Dame","Rois")


    def __init__(self,famille = 0,rang = 0):
        self.famille = famille
        self.rang = rang



    def __str__(self):
        return (self.rangs[self.rang] + " de " + self.familles[self.famille])


    def __repr__(self):

        return (self.rangs[self.rang] + " de " + self.familles[self.famille])

    def comp(self,other):
        if self.rang < other.rang : return -1
        if self.rang > other.rang : return 1
        return 0
    def __eq__(self,other):
        return self.comp(other) == 0

    def __ge__(self,other):
        return self.comp(other) >= 0

    def __gt__(self,other):
        return self.comp(other) > 0

    def __le__(self,other):
        return self.comp(other) <= 0

    def __lt__(self,other):
        return self.comp(other) < 0

    def __ne__(self,other):
        return self.comp(other) != 0

class Paquet:

    def __init__(self):
        self.paquet = []
        for famille in range(4):
            for rang in range(1,14):
                self.paquet.append(Cards(famille,rang))

    def __str__(self):
        string = ""
        espace = ""
        for carte in self.paquet:
            string = string + espace + str(carte) + "\n"
        return string

    def shuffle(self):
        import random
        for i in range(len(self.paquet)):
            random_number = random.randint(i,(len(self.paquet)-1))
            (self.paquet[random_number] , self.paquet[i]) = (self.paquet[i], self.paquet[random_number])

    def remove(self, card):
        if card in self.cards:
            self.cards.remove(card)
            return True
        else:
            return False

    def give(self):
        return self.cards.pop()

    def is_empty(self):
        return self.cards == []







card1 = Cards(1,5)
card2 = Cards(2,8)
paquet_bleu = Paquet()
paquet_bleu.shuffle()
print(paquet_bleu)

print(len(paquet_bleu.paquet))







class Formulaire :
    """Un formulaire quelconque pour m'entrainer"""
    def __init__(self):
        self.nom = str(input("Quel est votre nom?\n"))
        self.prenom =str(input("Quel est votre prenom?\n"))
        self.age = int(input("Quel est votre age?\n"))
        self.profession = str(input("Quel est votre profession?\n"))
        self.hobby = str(input("Quel est votre hobby favori?\n"))
        self.nom_du_chien =str(input("Quel est le nom de votre chien?\n"))

    def __str__(self):
        return "Nom :\t {0}\nPrenom :\t {1}\nAge :\t {2}\nProfession :\t {3}\nHobby :\t {4}\nNom du chien :\t {5}\n".format(\
        self.nom,self.prenom,self.age,self.profession,self.hobby,self.nom_du_chien)
