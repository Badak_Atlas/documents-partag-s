while 1 == 1 :
    try:
        variable1 = float(input("Numérateur?  "))
        variable2 = float(input("Dénominateur?  "))
        resultat_division = variable1 / variable2

    except ValueError:
        print("Veillez entrer un nombre valide")
    except ZeroDivisionError:
        print("Le dénominateur doit être différent de zéro")
    else:
        print ("Le résultat est", round (resultat_division,3)) #round arrondis on aurait put format (resultat_division) avex {0:.3f}
        break
    finally:
        pass
        "code executer au final whatever"
