class TableauNoir:
    """Classe définissant une surface sur laquelle on peut écrire,
    que l'on peut lire et effacer, par jeu de méthodes. L'attribut modifié
    est 'surface'"""


    def __init__(self):
        """Par défaut, notre surface est vide"""
        self.surface = ""
    def ecrire(self, message_a_ecrire):
        """Méthode permettant d'écrire sur la surface du tableau.
        Si la surface n'est pas vide, on saute une ligne avant de rajouter
        le message à écrire"""


        if self.surface != "":
            self.surface += "\n"
        self.surface += message_a_ecrire
    def lire(self):
        """Affiche le message enregistrer sur la surface du tableau"""


        print(self.surface)

    def effacer(self):
        """Efface la surface du tableau"""

        self.surface = ""
tab = TableauNoir()
chaine = "La sang de tes morts jullo"
tab.ecrire(chaine)
tab.lire()
tab.effacer()
help(TableauNoir)
tab.lire()
