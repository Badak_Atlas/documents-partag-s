
def anneeBissesxtile(annee_a_tester):
    """
        Test si l'année entrée est bissextile ou non.

        Pre : L'année entrée est un input;

        Post : Renvoie un string vous informant si elle est bissextile ou ne l'est pas
        """

    try:
        annee_a_tester = int(annee_a_tester)
        assert annee_a_tester > 0

    except ValueError:
        print ("ERROR : Veuillez entrer une année valide")

    except AssertionError:
        print ("ERROR : L'année saisie est égal ou inférieure a zéro")

    else:

        if (annee_a_tester % 400 == 0) or (annee_a_tester % 4 == 0 and annee_a_tester % 100 != 0):
            return "Elle est bissextile"
        else:
            return "Elle n'est pas bissextile"
