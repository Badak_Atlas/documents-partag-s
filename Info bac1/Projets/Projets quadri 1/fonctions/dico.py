score = {"joueur 1":    5,"joueur 2":   35,"joueur 3":   20,"joueur 4":    2, }
dico = {('A', 1): 'Tour Blanche', ('A', 2): 'Pion Blanc'}
dico["B", 1] = "Cavalier Blanc"
score["numpad 1"] = 5 #ajoute un élément

print (dico)
print (dico["A",1])
print (score)

for i in dico.keys():
    print(i)
for i in dico.values():
    print(i)
del dico["A",1]
for i in dico.values():
    print(i)

for i,a in dico.items():
    print("A la case {} se trouve la pièce {}".format(i,a))

def fonction_inconnue(**parametres_nommes):
     """Fonction permettant de voir comment récupérer les paramètres nommés
     dans un dictionnaire"""


     print("J'ai reçu en paramètres nommés : {}.".format(parametres_nommes))

fonction_inconnue() # Aucun paramètre

fonction_inconnue(p=4, j=8)
