
def diviseur():
    """
        Propose à l'utilisateur de diviser deux nombres

        Pre : /

        post : /
    """


    variable1 = 0.0
    variable2 = 0.0
    resultat_division = 0.0

    while 1 == 1 :
        try:
            variable1 = float(input("Numérateur?  "))
            variable2 = float(input("Dénominateur?  "))
            resultat_division = variable1 / variable2

        except ValueError:
            print("Veillez entrer un nombre valide")
        except ZeroDivisionError:
            print("Le dénominateur doit être différent de zéro")
        else:
            print ("Le résultat est", round (resultat_division,3))
            break
