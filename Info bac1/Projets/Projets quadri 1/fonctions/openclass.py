class Point:
    """Stock les coordonnées d'un point en x,y"""
    def __init__(self,x,y):
        """Constructeur du point"""
        self.x = x
        self.y = y
    def get_coordonnees(self):
        """Affiche les coordonnées du point"""
        print("Le point se situe en (",self.x,";",self.y,")")
