from random import*
import argparse
"""
    Travail de Vanloocke Arthur dans le cadre du cours "LINFO1102 - Projets en informatique 1" Groupe P4.1.6

    But: Imitation de la command "Shuf" de Unix en python 3.  //!\\ Seul les arguments -r -n -o  sont utilisables.

    Utilisation : %Run Shuf.py <args> <filename>
  
"""
#Récupération des arguments
parser = argparse.ArgumentParser(description="Permutte de manière aléatoire les lignes d'un fichier ")

#ces trois arguments sont falcultatifs
parser.add_argument("-r","--repeat",help="Permet de répeter les lignes précédemments affichées",action="store_true")
parser.add_argument('-n', '--lines', metavar='N', type=int, help='Afficher les N premières lignes.')
parser.add_argument("-o",'--destination',metavar='destination',type=str, nargs='+',help="Change la destination stantard d'écriture et la créé si elle n'existe pas")

#cet argument est obligatoire
parser.add_argument('fichier', metavar='fichier', type=str, nargs='+', help='Le fichier à afficher.')

args = parser.parse_args()


#Le code est établi de manière assez simple avec une condition pour chaque argument

#L'on regarde en premier si l'on veut pouvoir répéter les lignes
if args.repeat:
    
    
    #Ensuite si l'on veut travailler sur un nombre précis de ligne
    if args.lines:
        
        #A ce stade l'on stock provisoirement le contenu du fichier pour en connaître la taille
        lines1 = list()
        for i, fichier in enumerate(args.fichier):
            with open (fichier,"r")  as f:
                lines1 = f.readlines()
                    
        
        #L'on distingue deux cas:
        
        #Le premier est celui ou l'on a naturellement assez de lignes dans le fichier
        if args.lines <= len(lines1):
            
            for i, fichier in enumerate(args.fichier):
                
                with open (fichier,"r")  as f:
                    array1 = list()

                    for i in range (args.lines):
                        array1.append(lines1[i].strip())
                    shuffle(array1)

           #L'on regarde si la destination change
            if args.destination:
            
                for i, fichier in enumerate(args.destination):

                    with open (fichier,"w") as f:

                        for i in range (args.lines-1):
                            f.write(array1[i])
                            f.write("\n")
                        f.write(array1[-1])
                        

                        
           #Si elle ne change pas l'on affiche simplement le résultat                
            else:
                for i in range (args.lines):
                    print(array1[i])
        #Le deuxième cas est celui ou l'on n'a pas assez de lignes dans le fichier                                             
        else:
                
                for i, fichier in enumerate(args.fichier):

                    with open(fichier,"r")  as f:
                        array1 = []
                        for line in f.readlines():
                            array1.append(line.strip())
                        shuffle(array1)
               
               #L'on commence par répéter les lignes  pour pouvoir en afficher suffisamment 
                
                array3 = array1
                while len(array3) < args.lines:
                    array3 += array1
                                 
                
                #L'on regarde si la destination change
                if args.destination:
                    #On écrit le résultat dans le fichier de destination
                   for i, fichier in enumerate(args.destination):
                       with open (fichier,"w") as f:
                           for i in range(args.lines-1):          
                               f.write(array3[i])
                               f.write("\n")
                           f.write(array3[-1])
                          
               
               #Si elle ne change pas l'on affiche simplement le résultat        
                else:
                    for i in range(args.lines):
                        print(array3[i])
                    
                        
    #Ce cas est particulier car il créé une boucle infinie (unix également)
    #il s'agit du cas -r en absence de -n
    #si on ne limite pas le repeat il continue indéfiniment 
    else:
        if args.destination:
            print("Attention il s'agit d'une boucle infinie")
            for i, fichier in enumerate(args.destination):

                with open(fichier,"r")  as f:
                    array1 = []
                    for line in f.readlines():
                        array1.append(line.strip())
                    shuffle(array1)

                with open (fichier,"w") as f:

                    while 1==1:
                        for i in range(len(array1)-1):
                            f.write(array1[i])
                            f.write("\n")
                            
        else:
            print("Attention il s'agit d'une boucle infinie")
            for i, fichier in enumerate(args.fichier):

                with open(fichier,"r")  as f:
                    array1 = []
                    for line in f.readlines():
                        array1.append(line.strip())
                    shuffle(array1)

                with open (fichier,"w") as f:

                    while 1==1:
                        for i in range(len(array1)):                          
                            print(array1[i])

#L'on rentre dans la partie des opérations sans l'argument -r
#Le code est quasiment identique seul quelques cas doivent êtres traités différement
else:
   
    
    if args.lines:
        #Si le N donner en input est trop grand cette fois si l'on a plus la possibilité de combler les lignes manquantes
        try:
            for i, fichier in enumerate(args.fichier):
                with open (fichier,"r")  as f:
                    
                    array1 = list()
                    lines1 = list()
                    
                    for line in f.readlines():
                            lines1.append(line.strip())
                        
                    
                    for i in range (args.lines):
                        array1.append(lines1[i].strip())
                    shuffle(array1)
                    
            if args.destination:
                for i, fichier in enumerate(args.destination):

                    with open (fichier,"w") as f:

                        for i in range (args.lines-1):
                            f.write(array1[i])
                            f.write("\n")             
                        f.write(array1[-1])
        
                                                     
            else:
                        
                for i in range (args.lines):
                    print(array1[i])
        except:
            print("ERRROR : La valeur N de l'argument -n est trop grande ou manquante.")
                                                  
    else:
        for i, fichier in enumerate(args.fichier):

            with open(fichier,"r")  as f:
                array1 = []
                    
                for line in f.readlines():
                    array1.append(line.strip())
                    
                shuffle(array1)
        
        
        if args.destination:
            
            
            for i, fichier in enumerate(args.destination):
                
                with open (fichier,"w") as f:
                    for i in range(len(array1)-1):
                        f.write(array1[i])
                        f.write("\n") 
                    f.write(array1[-1])
                    
        
        #Ce cas est le cas primaire SHuf.py
        else:
             
            for i in range(len(array1)): 
                print(array1[i])
                   
