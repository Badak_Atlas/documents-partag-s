import os
import subprocess
import unittest

test_txt = \
"""
a
b
c
d
e
f
g
h
i
j
k
l
m
n
o
p
"""
def shuf(fichier, n = 0 ,r = 0,o = 0):
    with open (fichier,"r")  as f:
        array1 = []
        for line in f.readlines():
            array1.append(line.strip())
        shuffle(array1)
    with open (fichier,"w") as f:
        for i in range(len(array1)):
            f.write(array1[i])
            f.write("\n")

class TestShuf(unittest.TestCase):
       
    def test_default(self):
        with open('test.txt', 'w') as f:
            f.write(test_txt)
        with open ("test.txt","r")  as f:
            lines1 = f.readlines()
            lines1.sort()
        shuf("test.txt")
        with open ("test.txt","r")  as f:    
            lines2 = f.readlines()
            lines2.sort()
        self.assertEqual(1,1)

unittest.main()