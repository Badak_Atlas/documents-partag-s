import os
import subprocess
import unittest

test_txt = \
"""a
b
c
d
e
f
g
h
i
j
k
l
m
n
o
p"""


def head(*args):
    try:
        return subprocess.check_output(['python3', 'head.py', *args], universal_newlines=True)
    except subprocess.CalledProcessError as e:
        return e.output

class TestHead(unittest.TestCase):
    def setUp(self):
        with open('test.txt', 'w') as f:
            f.write(test_txt)

    def tearDown(self):
        os.unlink('test.txt')

    def test_default(self):
        self.assertEqual(head('test.txt'), test_txt[:10 * 2])

    def test_lines(self):
        self.assertEqual(head('test.txt', '-n', '5'), test_txt[:5 * 2])

if __name__ == '__main__':
    unittest.main()
