var1 = 5
array2 = [4,5,3]


def exact_allocation(n_sieges, resultat_vote):
    """
    Calcule la repartition exacte des sièges
    pre: `n_sieges` représente le nombre de sièges disponibles
        et `resultat_vote` est un tableau du nombre de voies tel
        que `resultat_vote`[`i`] est le nombre de votes pour
        le parti à l indice `i`
    post: renvoie un tableau `t` tel que `t`[`i`] correspond
        à l allocation exacte du parti à l indice `i`
    """
    nombreDeVotes = 0
    proportion = 0
    nombreDeSieges = 0.000
    array1 = list()
    t = list()
    for i in resultat_vote:
        nombreDeVotes += i
    for i in resultat_vote:
        proportion = i/nombreDeVotes

        t.append(round(proportion*n_sieges,3))


    return t

print(exact_allocation(var1,array2))
